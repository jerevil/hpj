hpj
README
======


1. On the terminal, go on the base of your project and do: composer install

2. Set your vhost

3. Execute sql requests in the file "application/configs/database/hpj.sql"

4. Replace your database connection (line 9 in application.ini "doctrine.dsn")

5. Go on the url of your site
