<?php

namespace GuestBook\Services;

use GuestBook\Inputs\ConfirmationEmail\ConfirmationEmailInput;
use GuestBook\Models\GuestBookInterface;
use GuestBook\Models\GuestBookRepositoryInterface;
use Zend_Mail;

class GuestBookService
{
    /**
     * @var GuestBookRepositoryInterface
     */
    private $guestBookRepository;

    /**
     * @var Zend_Mail
     */
    private $mail;

    /**
     * @param GuestBookRepositoryInterface $guestBookRepository
     * @param Zend_Mail                    $mail
     */
    public function __construct(GuestBookRepositoryInterface $guestBookRepository, Zend_Mail $mail)
    {
        $this->guestBookRepository = $guestBookRepository;
        $this->mail                = $mail;
    }

    /**
     * Get All entries in the guestBook
     *
     * @return array|\Doctrine_Collection
     */
    public function getAllEntries()
    {
        return $this->guestBookRepository->fetchAll();
    }

    /**
     * @param array $fields
     *
     * @return GuestBookInterface
     */
    public function addEntry(array $fields = array())
    {
        $guestBook = $this->guestBookRepository->create($fields);

        if ($guestBook->isValid() === true) {
            $this->guestBookRepository->insert($guestBook);
        }

        return $guestBook;
    }

    /**
     * Send a confirmation email
     *
     * @param ConfirmationEmailInput $confirmationEmailInput Confirmation email input
     */
    public function sendConfirmationEmail(ConfirmationEmailInput $confirmationEmailInput)
    {
        $this->mail->setBodyHtml($confirmationEmailInput->getBodyEmailWithReplacedToken());
        $this->mail->setFrom('guestbook@example.com', 'GuestBook admin');
        $this->mail->addTo($confirmationEmailInput->getWriterEmail(), $confirmationEmailInput->getWriterFullName());
        $this->mail->addBcc($confirmationEmailInput->getBccEmail());
        $this->mail->setSubject('Your guest book message');
        $this->mail->send();
    }
}
