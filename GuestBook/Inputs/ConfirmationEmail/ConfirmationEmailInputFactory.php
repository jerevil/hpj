<?php

namespace GuestBook\Inputs\ConfirmationEmail;

use GuestBook\Models\GuestBookInterface;

class ConfirmationEmailInputFactory
{
    /**
     * @var ConfirmationEmailInput
     */
    private $confirmationEmailInput;

    /**
     * Constructor
     *
     * @param ConfirmationEmailInput $confirmationEmailInput
     */
    public function __construct(ConfirmationEmailInput $confirmationEmailInput)
    {
        $this->confirmationEmailInput = $confirmationEmailInput;
    }

    /**
     * @param GuestBookInterface $guestBook  GuestBook model
     * @param array              $parameters Email parameters
     *
     * @return ConfirmationEmailInput
     */
    public function create(GuestBookInterface $guestBook, array $parameters = array())
    {
        $adminEmail   = (isset($parameters['adminEmail']) === true ? $parameters['adminEmail'] : '');
        $emailContent = (isset($parameters['emailContent']) === true ? $parameters['emailContent'] : '');

        $this->confirmationEmailInput->setBccEmail($adminEmail);
        $this->confirmationEmailInput->setBodyEmail($emailContent);
        $this->confirmationEmailInput->setWriterEmail($guestBook->getEmail());
        $this->confirmationEmailInput->setWriterFullName($guestBook->getFullName());
        $this->confirmationEmailInput->setWriterPhone($guestBook->getPhone());
        $this->confirmationEmailInput->setComment($guestBook->getComment());

        return $this->confirmationEmailInput;
    }
}
