<?php

namespace GuestBook\Inputs\ConfirmationEmail;

class ConfirmationEmailInput
{
    /**
     * @var string
     */
    private $bccEmail;

    /**
     * @var string
     */
    private $bodyEmail;

    /**
     * @var string
     */
    private $writerEmail;

    /**
     * @var string
     */
    private $writerFullName;

    /**
     * @var string
     */
    private $writerPhone;

    /**
     * @var string
     */
    private $comment;

    /**
     * Set the Bcc email
     *
     * @param string $email
     */
    public function setBccEmail($email) {
        $this->bccEmail = $email;
    }

    /**
     * get the Bcc email
     *
     * @return string
     */
    public function getBccEmail() {
        return $this->bccEmail;
    }

    /**
     * Set the Body of email
     *
     * @param string $body
     */
    public function setBodyEmail($body) {
        $this->bodyEmail = $body;
    }

    /**
     * get the Body of email
     *
     * @return string
     */
    public function getBodyEmail() {
        return $this->bodyEmail;
    }

    /**
     * Replace token in the body message
     *
     * @return string
     */
    public function getBodyEmailWithReplacedToken()
    {
        $body = $this->getBodyEmail();
        $body = str_replace('|fullName|', $this->getWriterFullName(), $body);
        $body = str_replace('|comment|', nl2br($this->getComment()), $body);
        $body = str_replace('|email|', $this->getWriterEmail(), $body);
        $body = str_replace('|phone|', $this->getWriterPhone(), $body);

        return $body;
    }

    /**
     * Set the writer email
     *
     * @param string $email
     */
    public function setWriterEmail($email) {
        $this->writerEmail = $email;
    }

    /**
     * get the writer email
     *
     * @return string
     */
    public function getWriterEmail() {
        return $this->writerEmail;
    }

    /**
     * Set the writer full name
     *
     * @param string $fullName
     */
    public function setWriterFullName($fullName) {
        $this->writerFullName = $fullName;
    }

    /**
     * get the writer full name
     *
     * @return string
     */
    public function getWriterFullName() {
        return $this->writerFullName;
    }

    /**
     * Set the writer phone
     *
     * @param string $phone
     */
    public function setWriterPhone($phone) {
        $this->writerPhone = $phone;
    }

    /**
     * get the writer full name
     *
     * @return string
     */
    public function getWriterPhone() {
        return $this->writerPhone;
    }

    /**
     * Set the comment
     *
     * @param string $comment
     */
    public function setComment($comment) {
        $this->comment = $comment;
    }

    /**
     * get the comment
     *
     * @return string
     */
    public function getComment() {
        return $this->comment;
    }
}