<?php

namespace GuestBook\Models;

interface GuestBookRepositoryInterface
{
    /**
     * Insert
     *
     * @param guestBookInterface $guestBook
     * @return void
     */
    public function insert(guestBookInterface $guestBook);

    /**
     * fetch one by id
     *
     * @param integer $id
     * @return GuestBookInterface
     */
    public function fetchOneById($id);

    /**
     * fetch all
     *
     * @return array|\Doctrine_Collection
     */
    public function fetchAll();

    /**
     * create a model
     *
     * @return GuestBookInterface
     */
    public function create();
}
