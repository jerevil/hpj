<?php

namespace GuestBook\Models;

interface GuestBookInterface
{
    /**
     * Set the id
     *
     * @param integer $id
     */
    public function setId($id);

    /**
     * Get The id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set the name
     *
     * @param string $name
     */
    public function setName($name);

    /**
     * get the name
     *
     * @return string
     */
    public function getName();

    /**
     * Set the first name
     *
     * @param string $firstname
     */
    public function setFirstname($firstname);

    /**
     * get the first name
     *
     * @return string
     */
    public function getFirstname();

    /**
     * get the full name
     *
     * @return string
     */
    public function getFullName();

    /**
     * Set the email
     *
     * @param string $email
     */
    public function setEmail($email);

    /**
     * get the email
     *
     * @return string
     */
    public function getEmail();

    /**
     * Set the phone
     *
     * @param string $phone
     */
    public function setPhone($phone);

    /**
     * get the phone
     *
     * @return string
     */
    public function getPhone();

    /**
     * Set the comment
     *
     * @param string $comment
     */
    public function setComment($comment);

    /**
     * get the comment
     *
     * @return string
     */
    public function getComment();

    /**
     * Set the file name
     *
     * @param string $fileName
     */
    public function setFile($fileName);

    /**
     * get the file name
     *
     * @return string
     */
    public function getFile();

    /**
     * Set the created date
     *
     * @param \DateTime $createdAt
     */
    public function setCreated(\DateTime $createdAt);

    /**
     * get the comment
     *
     * @return \DateTime
     */
    public function getCreated();

    /**
     * Check if the object is valid
     *
     * @return boolean
     */
    public function isValid();
}
