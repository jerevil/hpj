<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * init Doctrine
     *
     * @return Doctrine_Manager
     */
    protected  function _initDoctrine()
    {
        $manager        = Doctrine_Manager::getInstance();
        $doctrineConfig = $this->getOption('doctrine');

        if (empty($doctrineConfig['models_path']) === true) {
            $doctrineConfig['models_path'] =  APPLICATION_PATH . '/models';
        }

        Doctrine_Manager::connection($doctrineConfig['dsn']);
        Doctrine::loadModels($doctrineConfig['models_path']);

        $manager->setAttribute(Doctrine_Core::ATTR_VALIDATE, Doctrine_Core::VALIDATE_ALL);

        return $manager;
    }

    /**
     * Init DI Container
     */
    protected function _initContainer()
    {
        $builder = new \DI\ContainerBuilder();
        $builder->useAnnotations(true);
        $builder->addDefinitions(APPLICATION_PATH . '/configs/DI.php');

        $container  = $builder->build();
        $dispatcher = Zend_Controller_Front::getInstance()->getDispatcher();
        $dispatcher->setContainer($container);
    }
}
