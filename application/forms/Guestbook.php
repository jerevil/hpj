<?php

class Application_Form_Guestbook extends Zend_Form
{
    /**
     * Build guestBook form
     *
     * @throws Zend_Form_Exception
     */
    public function init()
    {
        // Set the method for the display form to POST
        $this->setMethod('post');
        $this->setAttrib('id', 'guestBookForm');

        // Add name element
        $this->addElement(
            'text',
            'name',
            array(
                'label'                  => '* Your name:',
                'required'               => true,
                'filters'                => array('StringTrim'),
                'data-validation'        => 'length',
                'data-validation-length' => 'min1'
            )
        );

        // Add firstname element
        $this->addElement(
            'text',
            'firstname',
            array(
                'label'                  => '* Your firstname:',
                'required'               => true,
                'filters'                => array('StringTrim'),
                'data-validation'        => 'length',
                'data-validation-length' => 'min1'
            )
        );

        // Add email element
        $this->addElement(
            'text',
            'email',
            array(
                'label'                  => '* Your email address:',
                'required'               => true,
                'filters'                => array('StringTrim'),
                'data-validation'        => array('email', 'length'),
                'data-validation-length' => 'min1',
                'validators'             => array(
                    'EmailAddress',
                )
            )
        );

        // Add confirm email element
        $this->addElement(
            'text',
            'confirm_email',
            array(
                'label'                  => '* Confirm your email address:',
                'required'               => true,
                'filters'                => array('StringTrim'),
                'data-validation'        => array('email', 'length'),
                'data-validation-length' => 'min1',
                'validators'             => array(
                    'EmailAddress',
                    array('Identical', false, 'email')
                )
            )
        );

        // Add phone element
        $this->addElement(
            'text',
            'phone',
            array(
                'label'    => 'Your phone:',
                'required' => false,
                'filters'  => array('StringTrim')
            )
        );

        // Add the file element
        $this->addElement(
            'file',
            'file',
            array(
                'validators' => array(
                    array('Count', false, '1' ),
                    array('Size', false, '50MB'),
                    array('Extension', false, 'jpg,jpeg,tif,eps'),
                ),
                'required'    => false,
                'label'       => 'Photo (jpg/tif/eps)',
                'destination' => './media/images/upload'
            )
        );

        // Add the comment element
        $this->addElement(
            'textarea',
            'comment',
            array(
                'label'      => 'Please Comment:',
                'required'   => false,
                'validators' => array(
                    array('validator' => 'StringLength', 'options' => array(0, 500))
                )
            )
        );

        // Add the submit button
        $this->addElement(
            'submit',
            'submit',
            array(
                'ignore' => true,
                'label'  => 'Sign Guestbook',
            )
        );

        // Add CSRF protection
        $this->addElement('hash', 'csrf', array('ignore' => true));
    }
}
