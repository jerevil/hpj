<?php

use GuestBook\Models\GuestBookInterface;
use GuestBook\Models\GuestBookRepositoryInterface;

class Application_Model_GuestBookTable  extends Doctrine_Table implements GuestBookRepositoryInterface
{
    /**
     * Fetch all guestBooks
     *
     * @return array|Doctrine_Collection
     */
    public function fetchAll()
    {
        return $this->findAll();
    }

    /**
     * Fetch one guestBook by id
     *
     * @param integer $id
     * @return GuestBookInterface
     */
    public function fetchOneById($id)
    {
        return $this->fetchOneById($id);
    }

    /**
     * Insert
     *
     * @param GuestBookInterface $guestBook
     *
     * @return GuestBookInterface
     */
    public function insert(GuestBookInterface $guestBook)
    {
        $guestBook->save();
    }
}
