<?php

use \GuestBook\Models\GuestBookInterface;

class Application_Model_GuestBook  extends Doctrine_Record implements GuestBookInterface
{
    public function setTableDefinition()
    {
        $this->setTableName('guest_book');

        $this->hasColumn('id', 'integer', 11, array(
            'type' => 'integer',
            'length' => '11',
            'primary' => true,
        ));

        $this->hasColumn('name', 'string', 100, array(
            'type' => 'string',
            'length' => '100',
            'notnull' => true
        ));

        $this->hasColumn('firstname', 'string', 100, array(
            'type' => 'string',
            'length' => '100',
            'notblank' => true
        ));

        $this->hasColumn('email', 'string', 100, array(
            'type' => 'string',
            'length' => '100',
            'notnull' => true
        ));

        $this->hasColumn('phone', 'string', 15, array(
            'type' => 'string',
            'length' => '15'
        ));

        $this->hasColumn('file', 'string', 200, array(
            'type' => 'string',
            'length' => '200'
        ));

        $this->hasColumn('comment', 'longtext');

        $this->hasColumn('created_at', 'timestamp');
    }

    /**
     * Set the id
     *
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get The id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * get the name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the first name
     *
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * get the first name
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * get the full name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->getName() . ' ' . $this->getFirstname();
    }

    /**
     * Set the email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * get the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the phone
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * get the phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the comment
     *
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * get the comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set the file name
     *
     * @param string $fileName
     */
    public function setFile($fileName)
    {
        $this->file = $fileName;
    }

    /**
     * get the file name
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set the created date
     *
     * @param \DateTime $createdAt
     */
    public function setCreated(\DateTime $createdAt)
    {
        $this->comment = $createdAt;
    }

    /**
     * get the comment
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}
