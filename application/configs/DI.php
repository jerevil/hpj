<?php


return array(
    'GuestBook\Models\GuestBookRepositoryInterface' => function() {
        return \Doctrine_Core::getTable('Application_Model_GuestBook');
    }
);
