<?php

class GuestbookController extends Zend_Controller_Action
{
    /**
     * @Inject
     * @var \GuestBook\Services\GuestBookService
     */
    private $guestBookService;

    /**
     * @Inject
     * @var \GuestBook\Inputs\ConfirmationEmail\ConfirmationEmailInputFactory
     */
    private $confirmationEmailInputFactory;

    /**
     * @Inject
     * @var Application_Form_Guestbook
     */
    private $guestBookForm;

    /**
     * @var Bootstrap
     */
    private $bootstrap;

    /**
     * Init action in controller
     */
    public function init()
    {
        $this->bootstrap = $this->getInvokeArg('bootstrap');
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->view->entries  = $this->guestBookService->getAllEntries();
    }

    /**
     * Sign action
     *
     * @return mixed
     */
    public function signAction()
    {
        $request = $this->getRequest();

        if ($request->isPost() === true && $this->guestBookForm->isValid($request->getPost()) === true) {
            $newFileName  = $this->getNewFileName($this->guestBookForm->file->getFileName());

            $this->guestBookForm->file->addFilter('Rename', $newFileName);
            $this->guestBookForm->file->receive();

            $data         = $this->guestBookForm->getValues();
            $data['file'] = $newFileName;

            $guestBook = $this->guestBookService->addEntry($data);

            $parameters             = $this->bootstrap->getOption('parameters');
            $confirmationEmailInput = $this->confirmationEmailInputFactory->create($guestBook, $parameters);

            $this->guestBookService->sendConfirmationEmail($confirmationEmailInput);

            return $this->_helper->redirector('index');
        }

        $this->view->form = $this->guestBookForm;
    }

    /**
     * Generate an unique name for the uploaded file
     *
     * @param string $fileName
     *
     * @return string
     */
    private function getNewFileName($fileName)
    {
        $originalFilename = pathinfo($fileName);

        return 'file-' . uniqid() . '.' . $originalFilename['extension'];
    }
}
